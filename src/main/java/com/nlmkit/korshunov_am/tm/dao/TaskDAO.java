package com.nlmkit.korshunov_am.tm.dao;

import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskDAO {
    private List<Task> taskss = new ArrayList<>();

    public Task create(String name) {
        final Task task = new Task(name);
        taskss.add(task);
        return task;
    }

    public void clear() {
        taskss.clear();
    }

    public List<Task> findAll() {
        return taskss;
    }



}
